package br.com.sicredi.cooperative.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sicredi.client.ClientID;
import br.com.sicredi.client.IDOutput;
import br.com.sicredi.entity.Parttner;
import br.com.sicredi.repository.PartnerRepository;
import br.com.sicredi.service.business.ParttnerServiceImpl;
import br.com.sicredi.util.BusinessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PartnerServiceTest {

	@MockBean
	private PartnerRepository partnerRepository;

	@MockBean
	private ClientID cpfClient;

	@Autowired
	private ParttnerServiceImpl partnerService;

	final String cpf = "01123362050";

	@Test
	public void checkPartnerInexistentTest() {
		when(cpfClient.permissionVerifier(eq(cpf))).thenReturn(new IDOutput(IDOutput.CpfStatus.ABLE_TO_VOTE));
		when(partnerRepository.findByIdentity(eq(cpf))).thenReturn(Optional.empty());

		partnerService.checkPartner(cpf);

		verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
		verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
		verify(partnerRepository, times(1)).save(any(Parttner.class));
	}

	@Test
	public void checkPartnerExistentTest() {
		when(cpfClient.permissionVerifier(eq(cpf))).thenReturn(new IDOutput(IDOutput.CpfStatus.ABLE_TO_VOTE));
		when(partnerRepository.findByIdentity(eq(cpf))).thenReturn(Optional.of(new Parttner(1L, cpf)));

		partnerService.checkPartner(cpf);

		verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
		verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
		verify(partnerRepository, times(0)).save(any(Parttner.class));
	}

	@Test
	public void checkPartnerForbiddenTest() {
		when(cpfClient.permissionVerifier(eq(cpf))).thenReturn(new IDOutput(IDOutput.CpfStatus.UNABLE_TO_VOTE));

		try {
			partnerService.checkPartner(cpf);
		} catch (BusinessException sbe) {
			verify(partnerRepository, times(0)).findByIdentity(eq(cpf));
			verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
			verify(partnerRepository, times(0)).save(any(Parttner.class));
			assertEquals(HttpStatus.FORBIDDEN, sbe.getHttpStatus());
		}
	}


}
