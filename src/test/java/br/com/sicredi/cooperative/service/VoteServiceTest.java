package br.com.sicredi.cooperative.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sicredi.client.ClientID;
import br.com.sicredi.client.IDOutput;
import br.com.sicredi.entity.Discussion;
import br.com.sicredi.entity.Parttner;
import br.com.sicredi.entity.Session;
import br.com.sicredi.entity.Vote;
import br.com.sicredi.entity.dto.VoteDTO;
import br.com.sicredi.repository.DiscussionRepository;
import br.com.sicredi.repository.PartnerRepository;
import br.com.sicredi.repository.SessionRepository;
import br.com.sicredi.repository.VoteRepository;
import br.com.sicredi.service.business.VoteServiceImpl;
import br.com.sicredi.util.BusinessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class VoteServiceTest {

	@MockBean
	private DiscussionRepository discussionRepository;

	@MockBean
	private SessionRepository sessionRepository;

	@MockBean
	private VoteRepository voteRepository;

	@MockBean
	private PartnerRepository partnerRepository;

	@MockBean
	private ClientID cpfClient;

	@Autowired
	private VoteServiceImpl voteService;

	final String cpf = "01123362050";

	@Before
	public void setUp() {
		when(cpfClient.permissionVerifier(eq(cpf))).thenReturn(new IDOutput(IDOutput.CpfStatus.ABLE_TO_VOTE));
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Testing", "Testing ok", 1L)));
		when(sessionRepository.findByDiscussionId(eq(1L))).thenReturn(Optional.of(new Session(1L, 1L, 123456789L, 123456789L, Boolean.TRUE)));
		when(voteRepository.findBySessionIdAndPartnerId(eq(1L), eq(1L))).thenReturn(Optional.empty());
		when(partnerRepository.findByIdentity(eq(cpf))).thenReturn(Optional.of(new Parttner(1L, cpf)));
		when(voteRepository.save(any(Vote.class))).thenReturn(new Vote());
	}

	@Test
	public void createVoteOkTest() {
		voteService.createVote(1L, new VoteDTO(1L, 1L, null, Boolean.TRUE, cpf));

		verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
		verify(discussionRepository, times(1)).findById(eq(1L));
		verify(sessionRepository, times(1)).findByDiscussionId(eq(1L));
		verify(voteRepository, times(1)).findBySessionIdAndPartnerId(eq(1L), eq(1L));
		verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
		verify(voteRepository, times(1)).save(any(Vote.class));
	}

	@Test
	public void createVoteConflictTest() {
		when(voteRepository.findBySessionIdAndPartnerId(eq(1L), eq(1L))).thenReturn(Optional.of(new Vote()));

		try {
			voteService.createVote(1L, new VoteDTO(1L, 1L, null, Boolean.TRUE, cpf));
		} catch (BusinessException sbe) {
			verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
			verify(discussionRepository, times(1)).findById(eq(1L));
			verify(sessionRepository, times(1)).findByDiscussionId(eq(1L));
			verify(voteRepository, times(1)).findBySessionIdAndPartnerId(eq(1L), eq(1L));
			verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
			verify(voteRepository, times(0)).save(any(Vote.class));
			assertEquals(HttpStatus.CONFLICT, sbe.getHttpStatus());
		}

	}

	@Test
	public void createVoteNotFoundTest() {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.empty());

		try {
			voteService.createVote(1L, new VoteDTO(1L, 1L, null, Boolean.TRUE, cpf));
		} catch (BusinessException sbe) {
			verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
			verify(discussionRepository, times(1)).findById(eq(1L));
			verify(sessionRepository, times(0)).findByDiscussionId(eq(1L));
			verify(voteRepository, times(0)).findBySessionIdAndPartnerId(eq(1L), eq(1L));
			verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
			verify(voteRepository, times(0)).save(any(Vote.class));
			assertEquals(HttpStatus.NOT_FOUND, sbe.getHttpStatus());
		}
	}

	@Test
	public void createVoteForbiddenCpfTest() {
		when(cpfClient.permissionVerifier(eq(cpf))).thenReturn(new IDOutput(IDOutput.CpfStatus.UNABLE_TO_VOTE));

		try {
			voteService.createVote(1L, new VoteDTO(1L, 1L, null, Boolean.TRUE, cpf));
		} catch (BusinessException sbe) {
			verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
			verify(discussionRepository, times(0)).findById(eq(1L));
			verify(sessionRepository, times(0)).findByDiscussionId(eq(1L));
			verify(voteRepository, times(0)).findBySessionIdAndPartnerId(eq(1L), eq(1L));
			verify(partnerRepository, times(0)).findByIdentity(eq(cpf));
			verify(voteRepository, times(0)).save(any(Vote.class));
			assertEquals(HttpStatus.FORBIDDEN, sbe.getHttpStatus());
		}
	}

	@Test
	public void createVoteForbiddenSessionTest() {
		when(sessionRepository.findByDiscussionId(eq(1L))).thenReturn(Optional.of(new Session(1L, 1L, 123456789L, 123456789L, Boolean.FALSE)));

		try {
			voteService.createVote(1L, new VoteDTO(1L, 1L, null, Boolean.TRUE, cpf));
		} catch (BusinessException sbe) {
			verify(cpfClient, times(1)).permissionVerifier(eq(cpf));
			verify(discussionRepository, times(1)).findById(eq(1L));
			verify(sessionRepository, times(1)).findByDiscussionId(eq(1L));
			verify(voteRepository, times(0)).findBySessionIdAndPartnerId(eq(1L), eq(1L));
			verify(partnerRepository, times(1)).findByIdentity(eq(cpf));
			verify(voteRepository, times(0)).save(any(Vote.class));
			assertEquals(HttpStatus.FORBIDDEN, sbe.getHttpStatus());
		}
	}

	@Test
	public void findVotesBySessionIdOkTest() {
		when(voteRepository.findBySessionId(eq(1L))).thenReturn(Arrays.asList(new Vote(), new Vote()));

		final List<VoteDTO> votesDto = voteService.findVotesBySessionId(1L);

		verify(voteRepository, times(1)).findBySessionId(eq(1L));
		assertEquals(2, votesDto.size());
	}

	@Test
	public void findVotesBySessionIdEmptyTest() {
		when(voteRepository.findBySessionId(eq(1L))).thenReturn(Arrays.asList());

		final List<VoteDTO> votesDto = voteService.findVotesBySessionId(1L);

		verify(voteRepository, times(1)).findBySessionId(eq(1L));
		assertEquals(0, votesDto.size());
	}
}
