package br.sicredi.service.interfaces;

import br.com.sicredi.entity.Discussion;
import br.com.sicredi.entity.dto.DiscussionDTO;
import br.com.sicredi.entity.dto.KeyDTO;

public interface DiscussionService {

	DiscussionDTO findDiscussion(Long id);

	KeyDTO createDiscussion(DiscussionDTO discussionDto);

	Discussion findById(Long id);

}