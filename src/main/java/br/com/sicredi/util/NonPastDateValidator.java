package br.com.sicredi.util;

import java.time.LocalDateTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NonPastDateValidator implements ConstraintValidator<NonPastDate, LocalDateTime> {

	@Override
	public boolean isValid(LocalDateTime value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}

		return LocalDateTime.now().isBefore(value);
	}


}
