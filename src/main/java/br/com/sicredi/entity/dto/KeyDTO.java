package br.com.sicredi.entity.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

public class KeyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	public KeyDTO() {
		super();
	}

	public KeyDTO(Long id) {
		this();
		this.id = id;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}
