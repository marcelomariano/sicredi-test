package br.com.sicredi.entity;

import java.io.Serializable;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("Partner")
public class Parttner implements Serializable {

	private static final long serialVersionUID = 1L;

	@Indexed
	private Long id;

	@Indexed
	private String cpf;

	public Parttner() {
		super();
	}

	public Parttner(Long id, String cpf) {
		this();
		this.id = id;
		this.cpf = cpf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}