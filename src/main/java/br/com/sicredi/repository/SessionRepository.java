package br.com.sicredi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.entity.Session;

@Repository
public interface SessionRepository extends CrudRepository<Session, Long> {

	Optional<Session> findByDiscussionId(Long discussionId);

	List<Session> findByOpen(Boolean open);


}
