package br.com.sicredi.service.business;

import java.util.Optional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.sicredi.client.ClientID;
import br.com.sicredi.client.IDOutput;
import br.com.sicredi.entity.Parttner;
import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.repository.PartnerRepository;
import br.com.sicredi.util.BusinessException;
import br.sicredi.service.interfaces.ParttnerService;

@Service
public class ParttnerServiceImpl implements ParttnerService {

	@Autowired
	private Logger logger;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private ClientID cpfClient;

	@Override
	public KeyDTO checkPartner(String cpf) {
		final IDOutput cpfOutput = cpfClient.permissionVerifier(cpf);

		if (cpfOutput.isUnableToVote()) {
			throw new BusinessException(HttpStatus.FORBIDDEN,
					"This ID cannothave permission to vote");
		}
		final Optional<Parttner> partnerOpt = partnerRepository
				.findByIdentity(cpf);

		if (!partnerOpt.isPresent()) {
			final Parttner partner = new Parttner(null, cpf);
			partnerRepository.save(partner);
			return new KeyDTO(partner.getId());
		}

		return new KeyDTO(partnerOpt.get().getId());
	}
}
