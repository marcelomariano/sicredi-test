package br.com.sicredi.service.business;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.sicredi.entity.Discussion;
import br.com.sicredi.entity.dto.DiscussionDTO;
import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.repository.DiscussionRepository;
import br.com.sicredi.util.BusinessException;
import br.sicredi.service.interfaces.DiscussionService;

@Service
public class DiscussionServiceImpl implements DiscussionService {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private DiscussionRepository discussionRepository;

	@Override
	public DiscussionDTO findDiscussion(Long id) {

		final Discussion discussionEntity = findById(id);

		final DiscussionDTO discussionDto = mapper.map(discussionEntity,
				DiscussionDTO.class);

		return discussionDto;
	}

	@Override
	public KeyDTO createDiscussion(DiscussionDTO discussionDto) {

		final Discussion discussionEntity = mapper.map(discussionDto,
				Discussion.class);
		discussionEntity.setCreated(System.currentTimeMillis());
		discussionRepository.save(discussionEntity);

		return new KeyDTO(discussionEntity.getId());
	}

	@Override
	public Discussion findById(Long id) {
		final Optional<Discussion> discussionOpt = discussionRepository
				.findById(id);

		if (!discussionOpt.isPresent()) {
			throw new BusinessException(HttpStatus.NOT_FOUND,
					"Pauta não encontrada.");
		}

		final Discussion discussion = discussionOpt.get();
		return discussion;
	}

}
