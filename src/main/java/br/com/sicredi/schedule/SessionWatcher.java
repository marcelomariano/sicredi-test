package br.com.sicredi.schedule;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.sicredi.client.RabbitClient;
import br.com.sicredi.entity.dto.SessionDTO;
import br.com.sicredi.service.business.SessionServiceImpl;

@Service
public class SessionWatcher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private SessionServiceImpl sessionService;

	@Autowired
	private RabbitClient rabbitClient;

	@Scheduled(cron = "*/3 * * * * *")
	public void verifyExpiredSessions() {
		final List<Long> ids = sessionService.closeSessions();

		ids.stream().forEach(
				i -> {
					final SessionDTO sessionDto = sessionService
							.findSessionSummaryByDiscussionId(i);
					rabbitClient.send(sessionDto);
				});
	}

}
