package br.com.sicredi.client;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.sicredi.util.IdentityDeserializer;
import br.com.sicredi.util.JsonUtils;

public class VoteIn implements Serializable {

	private static final long serialVersionUID = -3894047940451476583L;

	private Boolean value;
	private String cpf;

	public VoteIn() {
		super();
	}

	public VoteIn(Boolean value, String cpf) {
		this();
		this.value = value;
		this.cpf = cpf;
	}

	@NotNull(message = SessionIn.MANDATORY_FIELD_MSG)
	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	@JsonDeserialize(using = IdentityDeserializer.class)
	@CPF
	@NotBlank(message = SessionIn.MANDATORY_FIELD_MSG)
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
