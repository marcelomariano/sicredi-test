package br.com.sicredi.client;

import java.io.Serializable;

import org.springframework.data.annotation.Transient;

import br.com.sicredi.util.JsonUtils;

public class IDOutput implements Serializable {

	private static final long serialVersionUID = 5616117826692725396L;

	public enum CpfStatus {
		ABLE_TO_VOTE, UNABLE_TO_VOTE;
	}

	private CpfStatus status;

	public IDOutput() {
		super();
	}

	public IDOutput(CpfStatus status) {
		this();
		this.status = status;
	}

	public CpfStatus getStatus() {
		return status;
	}

	public void setStatus(CpfStatus status) {
		this.status = status;
	}

	@Transient
	public Boolean isUnableToVote() {
		return CpfStatus.UNABLE_TO_VOTE.equals(this.status);
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
