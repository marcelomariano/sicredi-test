package br.com.sicredi.client;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import br.com.sicredi.util.JsonUtils;

public class DiscussionIn implements Serializable {

	private static final long serialVersionUID = 7007395798695593123L;

	private String title;
	private String description;

	public DiscussionIn() {
		super();
	}

	public DiscussionIn(String title, String description, String creator) {
		this();
		this.title = title;
		this.description = description;
	}

	@NotBlank(message = SessionIn.MANDATORY_FIELD_MSG)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@NotBlank(message = SessionIn.MANDATORY_FIELD_MSG)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
