package br.com.sicredi.client;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.sicredi.util.JsonUtils;
import br.com.sicredi.util.DateDeserializer;
import br.com.sicredi.util.NonPastDate;

public class SessionIn implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String MANDATORY_FIELD_MSG = "Mandatory Field.";

	private LocalDateTime deadline;

	public SessionIn() {
		super();
	}

	public SessionIn(LocalDateTime created, LocalDateTime deadline, Boolean open, Long discussionId) {
		this();
		this.deadline = deadline;
	}

	@NonPastDate
	@JsonDeserialize(using = DateDeserializer.class)
	public LocalDateTime getDeadline() {
		return deadline;
	}

	public void setDeadline(LocalDateTime deadline) {
		this.deadline = deadline;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
