package br.com.sicredi.client;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sicredi.entity.dto.SessionDTO;
import br.com.sicredi.util.JsonUtils;

@Service
public class RabbitClient {

	@Autowired
	private Logger logger;

	@Autowired
	private AmqpTemplate amqpTemplate;

	public void send(SessionDTO session) {
		final String message = JsonUtils.logJson(session);
		logger.info(StringUtils.join("MQ in message", message));

		amqpTemplate.convertAndSend(message);

		logger.info(StringUtils.join("MQ out message", message));
	}
}
